var express = require('express');

var app = express();

var db = require('./db.js');

var bodyParser= require('body-parser');

app.use(bodyParser.json());

app.get('/', function (req, res) {

	res.send('Hello World   !')

})

app.get('/:app_code', function (req, res) {

        db.query("SELECT * FROM records where app_code = $1",[req.params.app_code],function(err,result){
	if(!err){
		res.json(result.rows)
	}else{
		res.send("Error a la DB");
		console.log(err);
	}
	});
})

app.get('/all_aplications', function (req, res) {

        db.query("SELECT * FROM app",function(err,result){
        if(!err){
                res.json(result.rows)
        }else{
                res.send("Error a la DB");
                console.log(err);
        }
        });
})

app.post('/:app_codep', function (req, res) {
	
        db.query("INSERT INTO records (app_code,player,score) values ($1,$2,$3)",[req.params.app_codep,req.body.player,req.body.score],function(err,result){
        if(!err){
                res.json(result.rows)
        }else{
                res.send("Error a la DB");
                console.log(err);
        }
        });
})


app.listen( process.env.PORT || 3000, function () {

	console.log('Example app listening on port 3000!' )

})
