var pg = require('pg');
exports.query = function(query, params, callback) {
  var connectionString = "postgres://jnwrpelpgkavim:c20047313effdbc44173d51042ef587c32cfa77edc524f07f5cdc29d9dd46f15@ec2-54-75-237-110.eu-west-1.compute.amazonaws.com:5432/d1p571i5eoib45";
  if (typeof params == "function") {
    callback = params;
    params = [];
  }
  pg.connect(connectionString  + '?ssl=true', function(err, client, done) {
    if (!err) {
      console.info(query);
      client.query(query, params, function(err, result) {
        done();
        callback(err, result);
      })
    } else {
      callback(err);
      return console.error(err);
    }
  });
};
